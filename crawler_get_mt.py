'''
  Created on Mar 18, 2013
  
  @author: _Qman
'''

#python standard lib modules load
import concurrent.futures
import urllib.request

#personal modules load
from wc_parser import url_parser

#load a single URL and return page content
def load_url(url, timeout):
  conn = urllib.request.urlopen(url, timeout=timeout)
  return conn.readall()

def query_urls_mt(URLS):
  #init future with up to 5 threads
  data_url = {}
  with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    #send the urls into the future threads
    future_to_url = {executor.submit(load_url, url, 10):url for url in URLS}
    #loop through the completed jobs
    for future in concurrent.futures.as_completed(future_to_url):
      url = future_to_url[future]
      try:
        data = future.result()
      except Exception as exc:
        print('%r generated an exception: %s' % (url, exc))
      else:
        data_url[url] = [url_parser.find_all_url(data), data]
  
  return data_url