'''
  Created on Mar 18, 2013
  
  @author: _Qman
'''

import crawler_get_mt as crawler_get

#import concurrent.futures

def main():
  
  print("========START MAIN LOOP========\n")
  
  URLS = ['http://www.foxnews.com/',
        'http://www.cnn.com/',
        'http://europe.wsj.com/',
        'http://www.bbc.co.uk/',
        'http://news.ycombinator.com/',
        'http://slashdot.org/'
        ]
  
  i = 0
  while i < 1000:
    data_queried = crawler_get.query_urls_mt(URLS)
    nURLS = []
    for start_url in URLS:
      try:
        for gotten_url in data_queried[start_url][0]:
          nURLS.append(gotten_url)
      except:
        print('Could not use url: %s will remove it from list' % gotten_url)
      else:
        URLS = nURLS
        i += 1
        print("Cycle: %i | Number of url: %i" % (i, len(URLS)))
  print("========RESULT========\n")
  print(URLS)
  print(" %d" % len(URLS))
  print("\n========END PROGRAM========")

  
if __name__ == '__main__':
  main()