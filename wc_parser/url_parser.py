'''
  Created on Aug 22, 2012
  
  @author: _Qman
'''

#find <a href= tag in html to get urls
def get_next_target(s):
  start_link = s.find('<a href=')
  
  if start_link == -1:
    return None, 0
  
  else:
    start_quote = s.find('"', start_link)
    end_quote = s.find('"', start_quote + 1)
    url = s[start_quote + 1: end_quote]
    
    return url, end_quote


#remove anything that is not http, https or ftp url
def check_url_validity(url):
  if (url[:3] in ['htt', 'ftp']):
    return True
  else:
    return False


def find_all_url(data):
  content = str(data)
  url_store = []
  
  while True:
    url, endpos = get_next_target(content)
    if url is not None and check_url_validity(url) is True:
      url_store.append(url)
      content = content[endpos:]
    else:
      break
  
  return url_store

def main():
  from os.path import join
  test_file = open(join('testing', 'sample_file_url.html')).read()
  #print(test_file)
  #print find_url(page)
  print(find_all_url(test_file))
  return None


if __name__ == '__main__':
  main()