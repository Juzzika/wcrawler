'''
  Created on Mar 18, 2013
  
  @author: _Qman
'''

from os.path import exists

class HTMLParser(object):
  def __init__(self, content):
    #force content to be as string
    self.page_content = str(content)
    
  def get_tags(self, tags_file = 'html_tags', custom_tags = None):
    if exists(tags_file) is False:
      return 'Cannot find html tags file libs'
    
    else:
      self.html_tags = self.__html_tags_formatting(tags_file)
      
  def __html_tags_formatting(self, file):
    content = open(file)
    tags = []
    for item in content.readlines():
      tags.append(item.strip())
      
    return tags
  
  def get_body(self):
    
    body_content = self.page_content
    
    start_body = body_content.find('<body>')
    
    if start_body == -1:
      return None
    
    else:
      start_body_tag = body_content.find('', start_body)
      end_body_tag = body_content.find('</body>', start_body + 1)
      body = body_content[start_body_tag + 1: end_body_tag]
    
    return str(body)
    
def main():
  instance = HTMLParser('test')
  instance.get_tags()
  return None

if __name__ == '__main__':
  main()